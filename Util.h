#ifndef Util_h_
#define Util_h_

#include <iostream>

using namespace std;

void Binary(unsigned long Number, int Level = 0) {
	unsigned long Remainder;

	if(Level == 63) {
		cout << Number;
	} else {
		Remainder = Number % 2;
		Binary(Number >> 1, ++Level);    
		if(((Level) % 8) == 0) cout << endl;
		cout << Remainder;
	}
	if(Level == 1) cout << endl;
	if(Level == 1) cout << endl;
}

#endif
