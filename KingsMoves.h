#ifndef KingsMoves_h_
#define KingsMoves_h_

	#include <iostream>
	
	using namespace std;

		class KingsMoves {


			public:

				KingsMoves();
				~KingsMoves();
				long GetMoves(int Square);

			protected:


			private:
				void Init();

				unsigned long Moves[64];

		};

#endif
