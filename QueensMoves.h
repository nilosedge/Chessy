#ifndef QueensMoves_h_
#define QueensMoves_h_

	#include <iostream>
	
	using namespace std;

		class QueensMoves {


			public:

				QueensMoves();
				~QueensMoves();
				long GetMoves(int Square);

			protected:


			private:
				void Init();

				unsigned long Moves[64];

		};

#endif
