#ifndef BishopsMoves_h_
#define BishopsMoves_h_

	#include <iostream>
	
	using namespace std;

		class BishopsMoves {


			public:

				BishopsMoves();
				~BishopsMoves();
				long GetMoves(int Square);

			protected:


			private:
				void Init();

				unsigned long Moves[64];

		};

#endif
