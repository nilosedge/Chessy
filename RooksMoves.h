#ifndef RooksMoves_h_
#define RooksMoves_h_

	#include <iostream>
	
	using namespace std;

		class RooksMoves {


			public:

				RooksMoves();
				~RooksMoves();
				long GetMoves(int Square);

			protected:


			private:
				void Init();

				unsigned long Moves[64];

		};

#endif
