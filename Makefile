CC = c++
LIBS =
INC =
LIB = QueensMoves.o RooksMoves.o BishopsMoves.o KingsMoves.o KnightsMoves.o
FLAGS = -Wall -pg
#FLAGS = -O5 -Wall -g
OP = -march=opteron

all: Chessy

run: Chessy
	./Chessy

%.o: %.cpp %.h
	$(CC) $(FLAGS) $(INC) $(OP) -c $<

Chessy: Chessy.cpp Util.h $(LIB)
	$(CC) $(FLAGS) -o $@ $(LIB) $(LIBS) $(INC) $<

clean:
	rm -fr *.o
	rm -fr Chessy
	rm -fr *.out
