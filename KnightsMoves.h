#ifndef KnightsMoves_h_
#define KnightsMoves_h_

	#include <iostream>
	
	using namespace std;

		class KnightsMoves {


			public:

				KnightsMoves();
				~KnightsMoves();
				long GetMoves(int Square);

			protected:


			private:
				void Init();

				unsigned long Moves[64];

		};

#endif
